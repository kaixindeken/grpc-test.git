package main

import (
	"gitee.com/kaixindeken/grpc-test/proto/project"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

const (
	port = ":13456"
)

type server struct{}

func main() {
	var err error
	//grpc
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Println("failed to listen: ", err.Error())
	}
	s := grpc.NewServer()
	project.RegisterHelloServiceServer(s, &server{})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Println("failed to serve: ", err.Error())
	}
}

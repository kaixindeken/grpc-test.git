package main

import (
	"context"
	"gitee.com/kaixindeken/grpc-test/proto/project"
)

func (s server) SayHello(ctx context.Context, request *project.Hello) (*project.Hi, error) {
	rsp := project.Hi{}
	if request.Hello == "hello" {
		rsp.Hi = "hi"
	} else {
		rsp.Hi = "hola"
	}
	return &rsp, nil
}

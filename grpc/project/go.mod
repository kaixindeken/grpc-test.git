module gitee.com/kaixindeken/grpc-test/grpc/project

go 1.17

require (
	gitee.com/kaixindeken/grpc-test/proto/project v0.0.0-20211110124217-fee21503bdb6
	google.golang.org/grpc v1.42.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20211109214657-ef0fda0de508 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)

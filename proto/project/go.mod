module gitee.com/kaixindeken/grpc-test/proto/project

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	golang.org/x/net v0.0.0-20211109214657-ef0fda0de508
	google.golang.org/grpc v1.42.0
)

require (
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
